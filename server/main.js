import { Meteor } from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import '/imports/models/auth/server'

const SEED_USERNAME = 'boris';
const SEED_PASSWORD = '12345';

Meteor.startup(() => {
    if (!Accounts.findUserByUsername(SEED_USERNAME)) {
        Accounts.createUser({
          username: SEED_USERNAME,
          password: SEED_PASSWORD,
        });
      }
});
