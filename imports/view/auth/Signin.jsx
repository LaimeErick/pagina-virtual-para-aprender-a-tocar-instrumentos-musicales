import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class Signin extends Component {
    constructor(props){
        super(props)
        this.state={
            username:'',
            password:''
        }
    }
    SubmitForlogin = (e) =>{
        e.preventDefault()
        console.log('ENVIANDO FORMULARIO')
        console.log(this.state.username)
        console.log(this.state.password)
    }
    componentDidMount(){
        import '/imports/assets/principal/js/jquery.vide'
        import initVideoback from '/imports/assets/principal/js/initVideoback'
        initVideoback()
    }
    render() {
        return (
            <div>
                <main className="login-body" data-vide-bg="/principal/video/login-bg.mp4">
 
                    <form className="form-default"onSubmit={this.SubmitForlogin}>

                        <div className="login-form">
                            
                            <div className="logo-login">
                                <a href="index.html"><img src="/principal/img/loder.ico" alt=""/></a>
                            </div>
                            <h2>Login Here</h2>
                            <div className="form-input">
                                <label htmlFor="name">Email</label>
                                <input type="email" name="email" placeholder="Email" onChange={e=> this.setState({username:e.target.value})} />
                            </div>
                            <div className="form-input">
                                <label htmlFor="name">Password</label>
                                <input type="password" name="password" placeholder="Password"onChange={e=> this.setState({password:e.target.value})}/>
                            </div>
                            <div className="form-input pt-30">
                                <input type="submit" name="submit" value="Iniciar Sesion"/>
                            </div>

                           
                            <a href="#" className="forget">Forget Password</a>
                            
                            <a href="register.html" className="registration">Registrar</a>
                        </div>
                    </form>
                    
                </main>
            </div>
        )
    }
}
