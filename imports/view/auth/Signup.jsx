import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor';

export default class Signup extends Component {
    constructor(props){
        super(props)
        this.state={
            nameComplete:'',
            email:'',
            username:'',
            password:'',
            re_password:''
        }
    }
    SubmitForRegister = (e) =>{
        e.preventDefault()
        console.log('ENVIANDO FORMULARIO DE REGISTRO')
        console.log(this.state)
        const form = {
            nameComplete:this.state.nameComplete,
            email:this.state.email,
            username:this.state.username,
            password:this.state.password,
            re_password:this.state.re_password
        }
        Meteor.call('aqui_voy_a_crear_usuarios',form,function(error,resp) {
            if(error){
                console.log(error)
            }else{
                console.log(resp)
            }
        })
    }
    componentDidMount(){
        import '/imports/assets/principal/js/jquery.vide'
        import initVideoback from '/imports/assets/principal/js/initVideoback'
        initVideoback()
    }
    render() {
        return (
            <div>
                <main className="login-body" data-vide-bg="/principal/video/login-bg.mp4">

                    <form onSubmit={this.SubmitForRegister}>

                        <div className="login-form" style={{width:'880px'}}>
                            <div className="logo-login">
                                <a href="index.html"><img src="/principal/img/loder.ico" alt="" /></a>
                            </div>
                            <h2>Registro de Usuarios</h2>
                            <div className='form-row form-input'>
                                <div className="form-group col-md-12">
                                    <label htmlFor="name">Nombre Completo</label>
                                    <input type="text" name="name" placeholder="Nombre Completo" autoComplete="off" onChange={e=> this.setState({nameComplete:e.target.value})}/>
                                </div>
                            </div>
                            <div className="form-row form-input">
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Correo Electronico</label>
                                    <input type="email" name="email" placeholder="Correo Electronico" autoComplete="off" onChange={e=> this.setState({email:e.target.value})}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Nombre de Usuario</label>
                                    <input type="text" name="username" placeholder="Nombre de Usuario" autoComplete="off" onChange={e=> this.setState({username:e.target.value})}/>
                                </div>
                            </div>
                            <div className="form-row form-input">
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Contraseña</label>
                                    <input type="password" name="password" placeholder="Contraseña" onChange={e=> this.setState({password:e.target.value})}/>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Confirmar Contraseña</label>
                                    <input type="password" name="re-password" placeholder="Confirmar Contraseña" onChange={e=> this.setState({re_password:e.target.value})}/>
                                </div>

                            </div>
                            <div className="form-input pt-30">
                                <input type="submit" name="submit" value="Registrar" />
                            </div>

                            <a href="login.html" className="registration">login</a>
                        </div>
                    </form>

                </main>

            </div>
        )
    }
}
