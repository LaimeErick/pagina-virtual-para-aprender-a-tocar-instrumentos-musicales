import React, { Component } from 'react'
import {Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Nav from '/imports/components/Nav'
import Sidebar from '/imports/components/Sidebar'
import Home from '/imports/view/home/Home'

export default class Layout extends Component {
    render() {
        const {routes} = this.props
        return (
            <div>
                <Nav/>
                <Sidebar/>
                <h1>Aqui estara nuestro dashboard</h1>
                <p>luego de haber iniciado sesion</p>
                <Switch>
                    {
                    routes.map((route,i)=>(
                        <SwitchRoutes key={i} {...route}/>))
                    }
                </Switch>
            </div>
        )
    }
}
