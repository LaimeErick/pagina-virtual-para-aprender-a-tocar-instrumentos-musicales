import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div>
                <h2>Este sera el contenido principal del dashboard</h2>
                <p>esta vista sera la primera que vera el usuario despues de iniciar sesion</p>
            </div>
        )
    }
}
