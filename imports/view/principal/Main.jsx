import React, { Component } from 'react'

export default class Main extends Component {
    render() {
        return (
            <div>
                 <div className="slider-area slider-bg ">
                     <div className="slider-active">

                         <div className="single-slider d-flex align-items-center slider-height ">
                             <div className="container">
                                 <div className="row align-items-center justify-content-between">
                                     <div className="col-xl-5 col-lg-5 col-md-9 ">
                                         <div className="hero__caption">
                                             <span data-animation="fadeInLeft" data-delay=".3s">Best Domain & hosting
                                                 service provider</span>
                                             <h1 data-animation="fadeInLeft" data-delay=".6s ">Aprende Musica
                                             </h1>
                                             <p data-animation="fadeInLeft" data-delay=".8s">Supercharge your
                                                 WordPress hosting with detailed
                                                 website analytics, marketing tools, security, and data
                                                 backups all in one place.</p>

                                             <div className="slider-btns">

                                                 <a data-animation="fadeInLeft" data-delay="1s" href="industries.html"
                                                     className="btn radius-btn">get started</a>
                                             </div>
                                         </div>
                                     </div>
                                     <div className="col-xl-6 col-lg-6">
                                         <div className="hero__img d-none d-lg-block f-right">
                                             <img src="/principal/img/hero_right.png" alt=""
                                                 data-animation="fadeInRight" data-delay="1s" />
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>

                         <div className="single-slider d-flex align-items-center slider-height ">
                             <div className="container">
                                 <div className="row align-items-center">
                                     <div className="col-lg-6 col-md-9 ">
                                         <div className="hero__caption">
                                             <span data-animation="fadeInLeft" data-delay=".3s">Best Domain & hosting
                                                 service provider</span>
                                             <h1 data-animation="fadeInLeft" data-delay=".6s">Powerful web hosting
                                             </h1>
                                             <p data-animation="fadeInLeft" data-delay=".8s">Supercharge your
                                                 WordPress hosting with detailed
                                                 website analytics, marketing tools, security, and data
                                                 backups all in one place.</p>

                                             <div className="slider-btns">

                                                 <a data-animation="fadeInLeft" data-delay="1s" href="industries.html"
                                                     className="btn radius-btn">get started</a>
                                             </div>
                                         </div>
                                     </div>
                                     <div className="col-lg-6">
                                         <div className="hero__img d-none d-lg-block f-right">
                                             <img src="/principal/img/hero_right.png" alt=""
                                                 data-animation="fadeInRight" data-delay="1s" />
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div className="slider-shape d-none d-lg-block">
                         <img className="slider-shape1" src="/principal/img/top-left-shape.png" alt="" />
                     </div>
                 </div>

                 <div className="domain-search-area section-bg1">
                     <div className="container">
                         <div className="row">
                             <div className="col-xl-4 col-lg-5">
                                 <h2>Search new domain</h2>
                                 <p>Supercharge your WordPress hosting with detailed website analytics, marketing
                                     tools.</p>
                             </div>
                             <div className="col-xl-8 col-lg-7">

                                 <form action="#" className="search-box">
                                     <div className="input-form">
                                         <input type="text" placeholder="Search for a domain" />

                                         <div className="search-form">
                                             <button><i className="ti-search"></i></button>
                                         </div>

                                         <div className="world-form">
                                             <i className="fas fa-globe"></i>
                                         </div>
                                     </div>
                                 </form>

                                 <div className="single-domain pt-30 pb-30">
                                     <ul>
                                         <li><span>.com</span>
                                             <p>$15.99</p>
                                         </li>
                                         <li><span>.net</span>
                                             <p>$10.99</p>
                                         </li>
                                         <li><span>.rog</span>
                                             <p>$10.99</p>
                                         </li>
                                         <li><span>.me</span>
                                             <p>$10.99</p>
                                         </li>
                                     </ul>
                                 </div>

                             </div>
                         </div>
                     </div>
                 </div>

                 <section className="team-area section-padding40 section-bg1">
                     <div className="container">
                         <div className="row justify-content-center">
                             <div className="col-xl-12">
                                 <div className="section-tittle text-center mb-105">
                                     <h2>Most amazing features</h2>
                                 </div>
                             </div>
                         </div>
                         <div className="row">
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className=" single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services1.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">Employee Owned</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className="single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services2.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">Commitment to Security</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className="single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services3.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">Passion for Privacy</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className="single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services4.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">Employee Owned</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className="single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services5.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">24/7 Support</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-4 col-md-4 col-sm-6">
                                 <div className="single-cat">
                                     <div className="cat-icon">
                                         <img src="/principal/img/services6.svg" alt="" />
                                     </div>
                                     <div className="cat-cap">
                                         <h5><a href="#">100% Uptime Guaranteed</a></h5>
                                         <p>Supercharge your WordPress hosting with detailed website analytics,
                                             marketing
                                             tools.</p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </section>

                 <section className="pricing-card-area fix">
                     <div className="container">
                         <div className="row justify-content-center">
                             <div className="col-xl-8 col-lg-8">
                                 <div className="section-tittle text-center mb-90">
                                     <h2>Choose plan which fit for you</h2>
                                     <p>Supercharge your WordPress hosting with detailed website analytics, marketing
                                         tools.
                                         Our experts are just part of the reason Bluehost is the ideal home for your
                                         WordPress website. We're here to help you succeed!</p>
                                 </div>
                             </div>
                         </div>
                         <div className="row">
                             <div className="col-xl-4 col-lg-4 col-md-6 col-sm-10">
                                 <div className="single-card text-center mb-30">
                                     <div className="card-top">
                                         <img src="/principal/img/price1.svg" alt="" />
                                         <h4>Shared Hosting</h4>
                                         <p>Starting at</p>
                                     </div>
                                     <div className="card-mid">
                                         <h4>$4.67 <span>/ month</span></h4>
                                     </div>
                                     <div className="card-bottom">
                                         <ul>
                                             <li>2 TB of space</li>
                                             <li>unlimited bandwidth</li>
                                             <li>full backup systems</li>
                                             <li>free domain</li>
                                             <li>unlimited database</li>
                                         </ul>
                                         <a href="#" className="borders-btn">Get Started</a>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-xl-4 col-lg-4 col-md-6 col-sm-10">
                                 <div className="single-card text-center mb-30">
                                     <div className="card-top">
                                         <img src="/principal/img/price2.svg" alt="" />
                                         <h4>Dedicated Hosting</h4>
                                         <p>Starting at</p>
                                     </div>
                                     <div className="card-mid">
                                         <h4>$4.67 <span>/ month</span></h4>
                                     </div>
                                     <div className="card-bottom">
                                         <ul>
                                             <li>2 TB of space</li>
                                             <li>unlimited bandwidth</li>
                                             <li>full backup systems</li>
                                             <li>free domain</li>
                                             <li>unlimited database</li>
                                         </ul>
                                         <a href="#" className="borders-btn">Get Started</a>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-xl-4 col-lg-4 col-md-6 col-sm-10">
                                 <div className="single-card text-center mb-30">
                                     <div className="card-top">
                                         <img src="/principal/img/price3.svg" alt="" />
                                         <h4>Cloud Hosting</h4>
                                         <p>Starting at</p>
                                     </div>
                                     <div className="card-mid">
                                         <h4>$4.67 <span>/ month</span></h4>
                                     </div>
                                     <div className="card-bottom">
                                         <ul>
                                             <li>2 TB of space</li>
                                             <li>unlimited bandwidth</li>
                                             <li>full backup systems</li>
                                             <li>free domain</li>
                                             <li>unlimited database</li>
                                         </ul>
                                         <a href="#" className="borders-btn">Get Started</a>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </section>

                 <div className="about-area1 section-padding40">
                     <div className="container">
                         <div className="row align-items-center justify-content-between">
                             <div className="col-xl-5 col-lg-5 col-md-8 col-sm-10">

                                 <div className="about-img ">
                                     <img src="/principal/img/about1.png" alt="" />
                                 </div>
                             </div>
                             <div className="col-xl-7 col-lg-7 col-md-12">
                                 <div className="about-caption ">

                                     <div className="section-tittle section-tittle2 mb-30">
                                         <h2>Global server location</h2>
                                     </div>
                                     <p className="mb-40">Supercharge your WordPress hosting with detailed website
                                         analytics,
                                         marketing tools. Our experts are just part of the reason Bluehost is the ideal
                                         home
                                         for your WordPress website. We're here to help you succeed!</p>
                                     <ul>
                                         <li>
                                             <img src="/principal/img/right.svg" alt="" />
                                             <p>WordPress hosting with detailed website</p>
                                         </li>
                                         <li>
                                             <img src="/principal/img/right.svg" alt="" />
                                             <p>Our experts are just part of the reason</p>
                                         </li>
                                         <li>
                                             <img src="/principal/img/right.svg" alt="" />
                                             <p> Detailed website analytics</p>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div className="about-area1 pb-bottom">
                     <div className="container">
                         <div className="row align-items-center justify-content-between">
                             <div className="col-xl-7 col-lg-7 col-md-12">
                                 <div className="about-caption about-caption3 mb-50">

                                     <div className="section-tittle section-tittle2 mb-30">
                                         <h2>Dedicated support</h2>
                                     </div>
                                     <p className="mb-40">Supercharge your WordPress hosting with detailed website
                                         analytics,
                                         marketing tools. Our experts are just part of the reason Bluehost is the ideal
                                         home
                                         for your WordPress website. We're here to help you succeed!</p>
                                     <ul className="mb-30">
                                         <li>
                                             <img src="/principal/img/right.svg" alt="" />
                                             <p>WordPress hosting with detailed website</p>
                                         </li>
                                         <li>
                                             <img src="/principal/img/right.svg" alt="" />
                                             <p>Our experts are just part of the reason</p>
                                         </li>
                                     </ul>
                                     <a href="#" className="btn"><i className="fas fa-phone-alt"></i>(10) 892-293
                                         2678</a>
                                 </div>
                             </div>
                             <div className="col-xl-5 col-lg-5 col-md-8 col-sm-10">

                                 <div className="about-img ">
                                     <img src="/principal/img/about2.png" alt="" />
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <section className="ask-questions section-bg1 section-padding30 fix">
                     <div className="container">
                         <div className="row justify-content-center">
                             <div className="col-xl-8 col-lg-9 col-md-10 ">

                                 <div className="section-tittle text-center mb-90">
                                     <h2>Frequently ask questions</h2>
                                     <p>Supercharge your WordPress hosting with detailed website analytics, marketing
                                         tools.
                                         Our experts are just part of the reason Bluehost is the ideal home for your
                                         WordPress website. We're here to help you succeed!</p>
                                 </div>
                             </div>
                         </div>
                         <div className="row">
                             <div className="col-lg-6 col-md-6">
                                 <div className="single-question d-flex mb-50">
                                     <span> Q.</span>
                                     <div className="pera">
                                         <h2>Why can't people connect to the web server on my PC?</h2>
                                         <p>We operate one of the most advanced 100 Gbit networks in the world, complete
                                             with
                                             Anycast support and extensive DDoS protection.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-6 col-md-6">
                                 <div className="single-question d-flex mb-50">
                                     <span> Q.</span>
                                     <div className="pera">
                                         <h2>What domain name should I choose for my site?</h2>
                                         <p>We operate one of the most advanced 100 Gbit networks in the world, complete
                                             with
                                             Anycast support and extensive DDoS protection.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-6 col-md-6">
                                 <div className="single-question d-flex mb-50">
                                     <span> Q.</span>
                                     <div className="pera">
                                         <h2>How can I make my website work without www. in front?</h2>
                                         <p>We operate one of the most advanced 100 Gbit networks in the world, complete
                                             with
                                             Anycast support and extensive DDoS protection.</p>
                                     </div>
                                 </div>
                             </div>
                             <div className="col-lg-6 col-md-6">
                                 <div className="single-question d-flex mb-50">
                                     <span> Q.</span>
                                     <div className="pera">
                                         <h2>Why does Internet Information Server want a password?</h2>
                                         <p>We operate one of the most advanced 100 Gbit networks in the world, complete
                                             with
                                             Anycast support and extensive DDoS protection.</p>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div className="row">
                             <div className="col-xl-12 ">
                                 <div className="more-btn text-center mt-20">
                                     <a href="#" className="btn">Go to Support</a>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </section>

                 <section className="testimonial-area section-bg1">
                     <div className="container">
                         <div className="testimonial-wrapper">
                             <div className="row align-items-center justify-content-center">
                                 <div className=" col-lg-10 col-md-12 col-sm-11">

                                     <div className="h1-testimonial-active">

                                         <div className="single-testimonial text-center mt-55">
                                             <div className="testimonial-caption">
                                                 <img src="/principal/img/quotes-sign.png" alt=""
                                                     className="quotes-sign" />
                                                 <p>Brook presents your services with flexible, convenient and cdpose
                                                     layouts. You can select your favorite layouts & elements for cular
                                                     ts
                                                     with unlimited ustomization possibilities. Pixel-perfect
                                                     replica;ition
                                                     of thei designers ijtls intended csents your se.</p>
                                             </div>

                                             <div
                                                 className="testimonial-founder d-flex align-items-center justify-content-center">
                                                 <div className="founder-img">
                                                     <img src="/principal/img/testimonial.png" alt="" />
                                                 </div>
                                                 <div className="founder-text">
                                                     <span>Jacson Miller</span>
                                                     <p>Designer @Colorlib</p>
                                                 </div>
                                             </div>
                                         </div>

                                         <div className="single-testimonial text-center mt-55">
                                             <div className=" testimonial-caption">
                                                 <img src="/principal/img/quotes-sign.png" alt=""
                                                     className="quotes-sign" />
                                                 <p>Brook presents your services with flexible, convenient and cdpose
                                                     layouts.
                                                     You can select your favorite layouts & elements for cular ts with
                                                     unlimited
                                                     ustomization possibilities. Pixel-perfect replica;ition of thei
                                                     designers
                                                     ijtls intended csents your se.</p>
                                             </div>

                                             <div
                                                 className="testimonial-founder d-flex align-items-center justify-content-center">
                                                 <div className="founder-img">
                                                     <img src="/principal/img/testimonial.png" alt="" />
                                                 </div>
                                                 <div className="founder-text">
                                                     <span>Jacson Miller</span>
                                                     <p>Designer @Colorlib</p>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                 </div>
                             </div>
                         </div>
                     </div>
                 </section>

                 </div>
        )
    }
}
