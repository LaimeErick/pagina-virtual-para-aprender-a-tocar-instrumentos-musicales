import React, { Component } from 'react'
import Principal from '/imports/view/principal/Principal'
import Signin from '/imports/view/auth/Signin'
import Signup from '/imports/view/auth/Signup'
import Layout from '/imports/view/layout/Layout'
import Main from '/imports/view/principal/Main'
import {useLocation} from 'react-router-dom'

function NoMatch(){
    let location = useLocation();
    return(
        <div>
            <h3>
                La pagina solicitada no existe <code>{location.pathname}</code>
            </h3>
        </div>
    );
}
const routes =[
    {
        path:'/Bienvenido',
        component:Principal,
        routes:[
            {
                path:'/Bienvenido/principal',
                component:Main
            },
            {
                path:'/Bienvenido/login',
                component:Signin
            },
            {
                path:'/Bienvenido/registro',
                component:Signup
            }
        ]
    },
    {
        path:'/dashboard',
        component:Layout,
        routes: [
            {
                path:'/register-user',
                component:Signin
            },
            {
                path:'/upload-multimedia',
                component:Signin
            }
        ]
    },
    {
        path:'*',
        component:NoMatch
    }
] 

export default routes